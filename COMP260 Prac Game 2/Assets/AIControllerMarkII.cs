﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class AIControllerMarkII : MonoBehaviour
{
    private Rigidbody rigidbody;
    public float speed = 25f;
    public Transform puck;
    public Transform defGoal;
    public bool onRoad;
    public bool finishHit;
    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //calculate the point between puck and gate
        //only if the puck is moving right and pass the middle the AI will shoot it 
        if (puck.position.x > 0)
        {
            Vector3 temp = defGoal.position - puck.position;
            if (temp.z > rigidbody.position.z)
            {
                rigidbody.velocity = new Vector3(0, 0, -1) * speed;
            }
            else if (temp.z < rigidbody.position.z)
            {
                rigidbody.velocity = new Vector3(0, 0, 1) * speed;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter(Collision collision)
    {
        this.onRoad = false;
    }
    public void stopSelf()
    {
        if (rigidbody)
        {
            rigidbody.position = new Vector3(3.8f, 0, 0);
            rigidbody.velocity = Vector3.zero;
            this.onRoad = true;
        }
    }
}
