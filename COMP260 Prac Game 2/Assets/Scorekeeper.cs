﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {
    public int scorePerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;
    public Text winMessage;
    public int pointsPerGoal = 1;
    public GameObject puck;
    static private Scorekeeper instance;
    // Use this for initialization
    void Start () {
        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError("More than one Scorekeeper exists in the scene.");
        }
        // reset the scores to zero
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }

    }

    // Update is called once per frame
    void Update () {
        if (score[0] > 4)
        {
            winMessage.text = "AI win";
            puck.SetActive(false);
        }
        if (score[1] > 4)
        {
            winMessage.text = "Human win";
            puck.SetActive(false);
        }
    }
    public void OnScoreGoal(int player)
    {
        score[player] += pointsPerGoal;
        scoreText[player].text = score[player].ToString();
    }
    static public Scorekeeper Instance
    {
        get { return instance; }
    }
}
